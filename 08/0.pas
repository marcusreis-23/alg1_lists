program maximizar_soma;

const MAX = 99;
var
 vetor,somas : array[0..MAX] of longint;
 lenv,lens,i : shortint;

function get_greatest(var arr : array of longint; arrlen : shortint) : longint;
var i : shortint;
begin
    get_greatest := arr[0];
    for i:=1 to arrlen-1 do 
        if arr[i] > get_greatest then
            get_greatest := arr[i];
end;

procedure generate_sums(var arr,sums : array of longint; arrlen : shortint; var sumlen : shortint);
var i,j : shortint;
begin
    for i:=0 to arrlen-1 do
    begin
        sums[sumlen]:=0;
        sumlen += 1;
        for j:=i to arrlen-1 do 
        begin
            sums[sumlen] := sums[sumlen-1]+arr[j];
            sumlen += 1;
        end;
    end;
end;
    
begin
    read(lenv);
    for i:=0 to lenv-1 do
        read(vetor[i]);
    if lenv = 0 then
        writeln('vetor vazio')
    else
    begin
        generate_sums(vetor,somas,lenv,lens);
        writeln(get_greatest(somas,lens));
    end;
end.
