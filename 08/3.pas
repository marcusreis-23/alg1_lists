program subsequences;

const MAX = 99;
var
 vetor : array[0..MAX] of longint;
 lenv,start,size,i : shortint;

(* checa se uma sequencia de um array que comeca no indice start-1 de tamanho size se
   repete depois no mesmo array *)
function repeats(var arr : array of longint; start,size,arrlen : shortint) : boolean;
var i,position : shortint;
begin
(* position: variavel separada para comecar a comparar a sequencia inicial
       com a nova, determinada pelo iterador i *)
    repeats := false; position := 0;
(* comecar do indice final da sequencia em questao *)
    i := start+size;
    while (i<arrlen) and not repeats do
    begin
(* termo "position" da sequencia eh igual ao que esta na frente *)
        if arr[start+position] = arr[i] then
        begin
            position += 1;
            if position = size then
                repeats := true;
        end
        else 
        begin
(* se a sequencia nao fechou eh preciso voltar, pois ha casas que podem 
   fazer parte da sequencia mas ja foram exploradas *)
            i -= position;
            position := 0;
        end; 
        i += 1; (* incrementa *)
    end;
end;

procedure repeatedsub(var arr : array of longint; var start,size : shortint; arrlen : shortint);
var i : shortint;
begin
    for i:=0 to arrlen-1 do
    begin
        if repeats(arr,start,size,arrlen) then
        begin
(* se a sequencia se repete vejamos se uma maior tambem se repete *)
            size += 1;
        end
(* se o tamanho da sequencia nao mudou ainda possivelmente nao achamos a sequencia *)
        else if size = 2 then
            start += 1
 (* ja foi encontrada a sequencia, nao eh mais preciso ficar no loop *)
        else break;
    end;
    size -= 1; (* tamanho esta incrementado um a mais *)
(* start esta no indice, que por questoes de padrao comeca em 0 nao em 1 *)
    start += 1; 
end;

begin
    read(lenv); start:=0; size:=2; (* tamanho minimo eh 2 *)
    for i:=0 to lenv-1 do
        read(vetor[i]);
    repeatedsub(vetor,start,size,lenv);
    if size >= 2 then
        writeln(start,' ',size)
    else
        writeln('nenhuma');
end.
