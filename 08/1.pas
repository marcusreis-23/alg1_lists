program compactacao;

const MAX = 99;
var
 linha,sem_rep : array[0..MAX] of longint;
 tam_linha,tam_srep,i : shortint;

function is_in_array(var arr : array of longint; arrlen : shortint; value : longint) : boolean;
var i : shortint; (* iterador *)
begin
    for i:= 0 to arrlen-1 do
    begin
        is_in_array := arr[i] = value;
        (* sai do loop imediatamente se ja estiver no array *)
        if is_in_array then break;
    end;
end;
        
(* "aumenta o comprimento do array" e adiciona o item na posicao de indice mais alto *)
procedure append_to_array(var arr : array of longint; var arrlen : shortint; value : longint);
begin
    if arrlen >= MAX then
        writeln('erro')
    else
    begin
        arr[arrlen] := value;
        arrlen += 1;
    end;
end;


begin
    repeat
        tam_srep := 0;
        read(tam_linha);
        if tam_linha > 0 then
        begin
            write('O: ');
            for i:=0 to tam_linha-1 do
            begin
                read(linha[i]);
                write(linha[i],' ');
                if not is_in_array(sem_rep,tam_srep,linha[i]) then
                    append_to_array(sem_rep,tam_srep,linha[i]);
            end;
            writeln();
            write('C: ');
            for i:=0 to tam_srep-1 do
                write(sem_rep[i],' '); 
            writeln();
        end;
    until tam_linha = 0;
end.
