program ocorrencias;

const MAX = 99;
var
 seq,sub : array[0..MAX] of longint;
 seql,subl,i,count,position : shortint;

begin
    read(seql,subl);
    for i:=0 to seql-1 do
        read(seq[i]);
    for i:=0 to subl-1 do
        read(sub[i]);
    position := 0; count := 0;
    for i:=0 to seql-1 do
    begin
        if sub[position] <> seq[i] then
            position := 0
        else if (sub[position] = seq[i]) and (position = subl-1) then
        begin
            count += 1;
            position := 0;
        end;
        position += 1;
    end;
    writeln(count);
end.
