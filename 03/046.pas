program media_ponderada;

var n,p,media,pesototal : longint;

begin
    media := 0;
    pesototal := 0;
    repeat
        read(n,p);
    (* o input zer0 "nao sera processado" pois seu valor eh
    justamente zero, de maneira a nao influenciar no valor final*)
        media += n*p;
        pesototal += p;
    until p = 0;
    writeln(media/pesototal:0:2);
end.
