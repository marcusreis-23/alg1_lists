program poligono_regular;

var 
 prev_side,next_side,counter : longint;
 regular : boolean;

begin
    read(prev_side);
    counter := 0;
    regular := true;
    repeat
        read(next_side);
        (* a partir do momento que o poligono for dito irregular
        nao ha como ele voltar a ser regular 
        (o mais ideal seria parar a leitura logo nesse momento
            ja que o resultado independe dos proximos valores) *)
        if regular and (next_side<>0)  then
        begin
            regular := next_side = prev_side;
            counter += 1;
        end;
    until next_side = 0;
    if regular and (counter>=3) then
        writeln('SIM')
    else
        writeln('NAO');
end.
