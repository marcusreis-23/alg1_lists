program produto_dos_impares;

var a,b,produto : longint;

begin
    read(a,b);
    produto := a;
    while a < b do
    begin
        a += 2;
        produto *= a;
    end;
    writeln(produto);
end.
