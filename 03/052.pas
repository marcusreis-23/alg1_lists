program media_aritmetica;

var 
 n,i : longint;
 numero,media : real;

begin
    read(n);
    media := 0;
    for i:=1 to n do
    begin
        read(numero);
        media += numero;
    end;
    writeln(trunc(media/n));
end.
