program saldos_negativos;

var saldo : real;

begin
    repeat
        read(saldo);
        if saldo < 0 then
            writeln(saldo:0:2);
    until saldo = 0;
end.
