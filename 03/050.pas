program count_divisions;

var num1,num2,counter : longint;

begin
    counter := 0;
    read(num1,num2);
    while num1 mod num2 = 0 do
    begin
        counter += 1;
        num1 := num1 div num2;
    end;
    writeln(counter);
end.
