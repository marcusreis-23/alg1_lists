program maior_multiplo_de_sete;

var numero,maior : longint;

begin
    maior := 0;
    repeat
        read(numero);
        if (numero mod 7 = 0) and (numero > maior) then
            maior := numero;
    until numero = 0;
    writeln(maior);
end.
