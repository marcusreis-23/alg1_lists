program digit_counter;

var n,m,counter : longint;

begin
    read(n,m);
    counter := 0;
    repeat
        (* ver se o primeiro digito eh igual a n *)
        if m mod 10 = n then
            counter += 1;
        (* remover o primeiro digito *)
        m := m div 10;
    until m=0;
    if counter = 0 then
        writeln('NAO')
    else
        writeln(counter);
end.
