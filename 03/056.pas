program soma_dos_pares_do_meio;

var n,m,i,soma : longint;

begin
    read(n,m);
    soma := 0;
    for i := n+1 to m-1 do
        if i mod 2 = 0 then
            soma += i;
    writeln(soma);
end.
