program censo_salarial;

const SALARIO_MINIMO = 450;

var 
 sub3,sub9,sub20,ovr20,counter : longint;
 salario : real;

begin
    counter := 0;
    sub3 := 0;
    sub9 := 0;
    sub20 := 0;
    ovr20 := 0;
    repeat
        read(salario);
        if salario > 0 then
        begin
            counter += 1;
            if salario<3*SALARIO_MINIMO then
                sub3 += 1
            else if salario < 9*SALARIO_MINIMO then
                sub9 += 1
            else if salario < 20*SALARIO_MINIMO then
                sub20 += 1
            else
                ovr20 += 1
        end;
    until salario = 0;
    writeln(sub3*100/counter:0:2);
    writeln(sub9*100/counter:0:2);
    writeln(sub20*100/counter:0:2);
    writeln(ovr20*100/counter:0:2);
end.
