program aprovacao_de_alunos;

var 
 notas : array[0..3] of real;
 media : real;
 faltas : longint;
begin
    read(notas[0],notas[1],notas[2],faltas);
    if faltas > 9 then
        begin
        writeln('NAO');
        exit;
        end;
    media := (notas[0]+notas[1]+notas[2])/3;
    if media >= 7 then
        writeln('SIM')
    else if media >=4 then
        writeln('TALVEZ')
    else 
        writeln('NAO');
end.
