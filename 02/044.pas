program calcula_intervalo;
var n,m,p,num : longint;
begin
    read(n,m,p);
{ Soma dos termos de uma P.A. = (a+an)*n/2 }
{ num = an (an nao necessariamente eh o mesmo que m) }
    num := (m-n) div p + 1;
    writeln((n+(n+(num-1)*p))*num div 2);
end.
