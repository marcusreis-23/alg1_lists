program palindromos;

var numero,invertido,tmpnum : longint;

begin
    read(numero);
    tmpnum := numero;
    invertido := 0;
    { inverter numero (com numero indefinido casas) }
    repeat
        { numero invertido recebe o proximo digito ao se
        multiplicar por dez e entao somar o digito em si,
        que sera o ultimo digito do numero original }
        invertido := invertido * 10 + tmpnum mod 10;
        { para conseguir o proximo digito eh necessario
        "remover" o ultimo digito do numero original, pois
        nessa sequencia de passos ele ja foi utilizado }
        tmpnum := tmpnum div 10;
    until tmpnum=0;
    if invertido = numero then
        writeln('SIM')
    else
        writeln('NAO');
end.
