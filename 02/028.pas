program farenheit_to_celsius_plus_water_state;

var farenheit : longint;

begin
    read(farenheit);
    writeln((5*farenheit - 160)/9:0:2);
    if farenheit <= 32 then
        writeln('solido')
    else if farenheit < 212 then
        writeln('liquido')
    else
        writeln('gasoso');
end.
