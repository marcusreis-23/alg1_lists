program fatorial_maior;
var max,fact,number : longint;
begin
    read(max);
    fact := 1; 
    number := 1;
    while fact<=max do
    begin
        fact := fact * (number+1);
        number += 1;
    end;
    writeln(number);
end.
