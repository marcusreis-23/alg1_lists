program display_greatest;
{ parametros variaveis possibilitam chamar o procedimento por referencia
possibilitando alterar os valores dos parametros em si, ao inves de uma copia }
procedure swap_values(var x,y : real);
var tmp : real;
begin
    tmp := x;
    x := y;
    y := tmp;
end;
var 
 a,b,c : real;
 i : longint;
begin
    read(i,a,b,c);
    if (i <> 3) then
    begin
        if (a>c) then
            swap_values(a,c);
        if (a>b) then
            swap_values(a,b);
        if (b>c) then
            swap_values(b,c);
    end
    { complicacao devido a necessidade de manter a ordem dos numeros }
    else 
        if (a>=b) and (a>=c) then
            begin
                swap_values(a,c);
        { como c vem depois de b, eh preciso inverter a e b, para manter a ordem }
                swap_values(a,b);
            end
        else if (b>=a) and (b>=c) then
            swap_values(b,c);
    case i of
        1: writeln(a:0:0,' ',b:0:0,' ',c:0:0);
        2: writeln(c:0:0,' ',b:0:0,' ',a:0:0);
        3: writeln(a:0:0,' ',c:0:0,' ',b:0:0);
    end;
end.
