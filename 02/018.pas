program custo_lanche;

var code,quantity : longint;

begin
    read(code,quantity);
    case (code) of
        100,103: writeln(quantity*1.2:0:2);
        102: writeln(quantity*1.5:0:2);
        101,104: writeln(quantity*1.3:0:2);
        105: writeln(quantity,'.00');
    end;
end.
