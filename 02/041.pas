program soma_fibonacci;
uses math;
var 
n: longint;
soma : real;

{
Segundo a Matematica Discreta basta encontrar o polinomio caracteristico da relacao
de recorrencia que da o resultado dos coeficientes para a expressao do termo Sn.
Por observacao, pecebe-se que o termo n da soma de termos fibonacci sera F(n+2)-1
Fibonacci: 0 1 1 2 3 5 8 13 ...
Soma     : 0 1 2 4 7 12 ...
sendo assim, S(n) = F(n+2) - 1

resolver o problema desse modo possibilita um tempo de execucao logaritmico, ficando
mais rapido do que uma solucao que utiliza de loops, que resulta em um tempo linear.
}

begin
    read(n);
    soma := ((power((1+sqrt(5))/2,n+1))-(power((1-sqrt(5))/2,n+1)))/sqrt(5)-1;
    writeln(soma:0:0);
end.
