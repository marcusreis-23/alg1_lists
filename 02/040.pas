program maior_e_menor;
var maior,menor,numero : longint;
begin
    read(numero);
    maior := numero;
    menor := numero;
    while numero <> 0 do
    begin
        if numero<menor then
            menor := numero;
        if numero>maior then
            maior := numero;
        read(numero);
    end;
    writeln(maior,' ',menor);
end.
