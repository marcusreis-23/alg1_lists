program soma_dos_impares;
var n,i,soma : longint;
begin
    soma := 0;
    read(n);
    for i:=1 to n do
    begin
    { progressao aritmetica dos numeros impares: an = 2n-1 }
        soma := soma + 2*i-1;
    end;
    writeln(soma);
end.
