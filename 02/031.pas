program credito_do_cliente;

var 
 saldo : real;
 porcentagem: integer;

begin
    read(saldo);
    if saldo <= 200 then
        porcentagem := 0
    else if saldo <= 400 then
        porcentagem := 20
    else if saldo <= 600 then
        porcentagem := 30
    else
        porcentagem := 40;
    writeln(saldo:0:0);
    write(porcentagem);
    if porcentagem <> 0 then
        writeln('%');
end.
