program preco_total;

var codigo,quantidade : longint;

begin
    read(codigo, quantidade);
    case (codigo) of
        1001,987: writeln(quantidade*5.32:0:2);
        1324,7623: writeln(quantidade*6.45:0:2);
        6548: writeln(quantidade*2.37:0:2);
    else
        writeln('ERRO');
    end;
end.
