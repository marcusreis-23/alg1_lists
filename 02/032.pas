program aumento_de_salario;

var 
 salario : real;
 codigo : integer;

begin
    read(salario, codigo);
    if (codigo<>101)and(codigo<>102)and(codigo<>103)then
        codigo := 104;
    writeln(salario:0:2);
    writeln(salario*(1+(codigo-100)/10):0:2);
    writeln(salario*((codigo-100))/10:0:2);
end.
