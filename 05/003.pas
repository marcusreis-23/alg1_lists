program serie;
const N = 5;
var
 numerador,denominador,i : longint;
 soma: real;

begin
    numerador := 1; denominador := 1;
    soma := 1;
    for i:=2 to N do
    begin
        numerador += denominador;
        denominador += numerador;
        soma += numerador/denominador;
    end;
    writeln(soma:0:2);
end.
