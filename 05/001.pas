program somador_serie;
uses math;
var numero,i : longint;
soma : real;

begin
    read(numero);
    soma := 0;
    for i:=1 to numero do
    begin
        soma += power(-1,i+1)*(1000-3*(i-1))/i;
    end;
    writeln(soma:0:2);
end.
