program serie_infinita;

procedure swapv(var num,den : longint);
var tmp : longint;
begin
    tmp := num+2;
    num := den+2;
    den := tmp;
end;
const N = 10;
var 
 numerador,denominador,i : longint;
 soma : real;
begin
    numerador := 1; denominador := 2; soma := 1/2;
    for i:=2 to N do
    begin
        swapv(numerador,denominador);   
        soma += numerador/denominador;
    end;
    writeln(soma:0:2);
end.

