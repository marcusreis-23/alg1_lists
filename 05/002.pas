program funny_sum;

var numero,i : longint;
soma : real;

begin
    read(numero);
    soma := 0;
    for i:=1 to numero-1 do
        soma += i/(numero-i);
    writeln(soma:0:2);
end.
