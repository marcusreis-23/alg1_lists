program serie_infinita;

procedure swapv(var num,den : longint);
var tmp : longint;
begin
    tmp := 2*num;
    num := 2*den;
    den := tmp;
end;

const N = 10;

var 
 numerador,denominador,i : longint;
 soma : real;
begin
    numerador := 1; denominador := 3; soma := 1/3;
    for i:=2 to N do
    begin
        swapv(numerador,denominador);   
        soma += numerador/denominador;
    end;
    writeln(soma:0:2);
end.
