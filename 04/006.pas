program numeros_perfeitos;
uses math;
function is_prime(const n: longint) : boolean;
var i : longint;
begin
    i:=2; 
    while i <= sqrt(n) do
    begin
        if (n mod i = 0) then
            is_prime := false;
        i+=1;
    end;
    is_prime := true;
end;

function is_perfect(const n: real) : boolean;
var i : longint;
soma : real;
begin
    i := 1; soma := 0;
    while i <= sqrt(n) do
    begin
        if n mod i = 0 then
        begin
            soma += i;
            if i <> n / i then
                soma += n / i;
        end;
        i+=1;
    end;
    is_perfect := (soma-n) = n;
end;

var i,numero : longint;
    formula : real;
(* numero perfeito tem forma 2^(p-1)*(2^p-1) em que p eh um numero primo *)
(* metodo mais eficiente: apenas verificar se um numero eh perfeito utilizando
a formula acima com uma sequencia de numeros primos *)
(* ineficiente para numeros >= 8 *)
begin
    read(numero);
    i:=1;
    while numero>0 do
    begin
       (* checar se i eh primo *)         
        if is_prime(i) then
        begin
            formula := power(2,i-1)*(power(2,i)-1);
            if is_perfect(formula) then
            begin
                write(formula:0:0,' ');
                numero -= 1;
            end;
        end;
        i+=1;
    end;
    writeln();
end.
