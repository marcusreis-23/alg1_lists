program soma_dos_algarismos;

const NUMERO = 37;
var multiplicando,soma,resultado : longint;

begin
    read(multiplicando);
    resultado := NUMERO * multiplicando;
    soma := 0;
    repeat
        soma += resultado mod 10;
        resultado := resultado div 10;
    until resultado = 0;
    if soma = multiplicando then
        writeln('SIM')
    else
        writeln('NAO');
end.
