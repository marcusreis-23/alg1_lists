program numero_primo;

var numero,i : longint;
is_prime : boolean;

begin
    read(numero);
    i := 2; is_prime := numero > 1;
    while i <= sqrt(numero) do
    begin
        if numero mod i = 0 then
        begin
            is_prime := false;
            break;
        end;
        i+=1;
    end;
    if is_prime then
        writeln('SIM')
    else
        writeln('NAO');
end.
