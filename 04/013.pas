program condicoes_bem_especificas;

var i,j,k : longint;

begin
    read(i,j,k);
    if (j mod i = 0) and (k mod j = 0) then
        writeln(i+j+k)
    else if (j = i+1) and (k = j+1) then
        writeln(k,' ',j,' ',i)
    else
        writeln((k+j+i)/3:0:0);
end.
