program numero_triangular;
var 
 numero,a,b,c,delta : longint;
 r1,r2 : real;
begin
    read(numero);
    (* um numero eh triangular se: num = (n+1)*n/2
    que eh na verdade uma equacao quadradica n^2 + n -2*num = 0 
    logo, basta encontrar alguma raiz real inteira da equacao *)
    a := 1; b := 1;
    c := -2 * numero;
    delta := b*b - 4*a*c;
    (* a raiz sempre sera real, pois delta nao podera ser negativo, ja que
    b^2 eh positivo, a sempre sera 1 e c sempre sera negativo, resultando em
    um numero real positivo, o que possibilita descartar a checagem de delta > 0 *)
    r1 := (-b+sqrt(delta))/(2*a);
    r2 := (-b-sqrt(delta))/(2*a);
    (* para saber se um numero eh inteiro eh preciso arrendonda-lo ou para
    cima ou para baixo, e se ele nao for mudado significa que ele ja era inteiro *)
    if ((r1>0) and (trunc(r1)=r1)) or ((r2>0) and (trunc(r2)=r2)) or (numero = 0) then
        writeln('1')
    else
        writeln('0');

end.
