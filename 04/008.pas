program very_specific_number_thingy;

var numero,anterior,soma,counter : longint;

begin
    read(anterior,numero);
    counter := 2; soma := anterior + numero;
    while (anterior <> 2*numero) and (numero <> 2*anterior) do
    begin
        anterior := numero;
        read(numero);
        soma += numero;
        counter += 1;
    end;
    writeln(counter,' ',soma,' ',anterior,' ',numero);
end.
