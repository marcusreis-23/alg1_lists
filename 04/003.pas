program serie_valor_quadrado;

var 
 numero,anterior : longint;
 valor_quadrado  : byte;

begin
    valor_quadrado := 1;
    while true do
    begin
        read(anterior);
        if anterior = 0 then break;
        read(numero); 
        if numero <> anterior*anterior then
            valor_quadrado := 0;
        anterior := numero; 
    end;
    writeln(valor_quadrado);
end.
