program will_surpass;

var 
 pa,pb : real;
 ta, tb : single;

function time_to_surpass(smaller,bigger : real; const rate1,rate2 : single) : longint;
var anos : longint;
begin
    anos := 0;
    repeat
        smaller *= rate1;
        bigger *= rate2;
        anos += 1;
    until smaller>bigger;
    time_to_surpass := anos;
end;

begin
    read(pa,pb,ta,tb);
    ta += 1; tb +=1;
    if (pa < pb) and (ta > tb) then
        writeln(time_to_surpass(pa,pb,ta,tb))
    else if (pb < pa) and (tb > ta) then
        writeln(time_to_surpass(pb,pa,tb,ta))
    else
        writeln('0');
end.
