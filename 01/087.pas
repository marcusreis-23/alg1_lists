program potencia;

const WATT_PER_M2 = 18;
var size1,size2,area : longint;

begin
    read(size1,size2);
    area := size1*size2;
    writeln(area,' ',area*WATT_PER_M2);
end.
