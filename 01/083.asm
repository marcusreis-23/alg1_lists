%include "basefunctions.asm"
SECTION .data
    space db " ",0x0

SECTION .bss
    num resb 10  ; int = 4 bytes

SECTION .text
    global _start

_start:
    mov eax,3
    mov ebx,0
    mov ecx,num 
    mov edx,10
    int 0x80
    
    mov eax,num
    call atoi
    mov edx,0
    mov ebx,365
    div ebx
    call iprint

    mov eax,space
    call sprint

    mov eax,edx
    mov ebx,30
    mov edx,0
    div ebx
    call iprint

    mov eax,space
    call sprint

    mov eax,edx
    call iprintln

    call quitprogram
