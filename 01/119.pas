program arithmetic_progression_from_k_term;

var k,ak,r,n : longint;

begin
    read(k,ak,r,n);
    writeln(ak+(n-k)*r);
end.
