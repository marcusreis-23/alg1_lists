program cubo_positivo_quadrado_negativo;

var numero : longint;

begin
    read(numero);
    if numero < 0 then
        writeln(numero*numero)
    else
        writeln(numero*numero*numero);
end.
