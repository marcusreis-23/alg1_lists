%include "basefunctions.asm"

SECTION .bss
    num resb 6

SECTION .text
    global _start

_start:
    mov eax,3
    mov ebx,0
    mov ecx,num
    mov edx,6
    int 0x80

    mov eax,num
    call atoi

    mov ebx,173
    mul ebx

    mov ebx,100
    div ebx

    call iprintln
    call quitprogram
