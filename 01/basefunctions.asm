; calculate string length ---------------
;
;
stringlen:
    push ebx                ; save ebx value
    mov ebx,eax             ; eax is expected to point to
                            ; the start of the string
nextchar:
    cmp byte [eax],0        ; check if eax points to terminating char
    jz endofstring          ; if it is get out of the loop
    inc eax                 ; make eax point to next adjacent character
    jmp nextchar            ; loop back

endofstring:
    sub eax,ebx             ; string length is stored in eax
    pop ebx                 ; restore ebx value
    ret

; print a string without line break -----
;
;
sprint:
    push edx                ; save register values for later use
    push ecx
    push ebx
    push eax                ; save string for later

    call stringlen 

    mov edx,eax             ; put string length in count
    pop eax                 ; get string back to eax

    mov ecx,eax             ; put string text in *buf
    mov eax,4               ; sys_write
    mov ebx,1               ; to stdout
    int 0x80

    pop ebx                 ; restore register values
    pop ecx
    pop edx

    ret


; print a string and break line --------
;
;
sprintln:
    call sprint             ; print string like we normally would

    push eax
    mov eax,0xa             ; 0xa = ASCII '\n'
    push eax                
    mov eax,esp
    call sprint             ; print '\n'
    pop eax
    pop eax
    ret

; print integer as string without line break ---
;
;
iprint:
    push eax                ; save register values
    push ecx
    push edx
    push esi
    mov ecx,0               ; start loop on 0

divloop:
    inc ecx
    mov edx,0               ; make sure edx is 0 before dividing
    mov esi,10              
    idiv esi                ; divide eax by esi (10)
    add edx,'0'
    push edx                ; push current char to stack
    cmp eax,0               ; if quotient is zero
    jnz divloop

printloop:                  ; prints all chars we put on the stack
    dec ecx                 ; ecx will have the number of digits to show
    mov eax,esp             ; eax points to current char now
    call sprint             
    pop eax                 ; put next digit in eax
    cmp ecx,0               ; if we just printed the last digit
    jnz printloop

    pop esi                 ; restore previous register values
    pop edx
    pop ecx
    pop eax
    ret

; print integer as string and break line -----
;
;
iprintln:
    call iprint             ; print integer normally

    push eax
    mov eax,0xa
    push eax
    mov eax,esp
    call sprint
    pop eax
    pop eax
    ret

atoi:
    push esi
    push edx                ; save register values for later use
    push ecx
    push ebx

    mov esi,eax
    mov eax,0
    mov ecx,0

.multiplyloop:
    xor ebx,ebx
    mov bl,[esi+ecx]
    cmp bl,48
    jl .finished
    cmp bl,57
    jg .finished

    sub bl,48
    add eax,ebx
    mov ebx,10
    mul ebx
    inc ecx
    jmp .multiplyloop

.finished:
    cmp eax,0
    je .restore
    mov ebx,10
    div ebx
.restore:
    pop ebx
    pop ecx
    pop edx
    pop esi
    ret

quitprogram:
    mov eax,1
    mov ebx,0
    int 0x80
    ret
