program weird_number_property;

var numero,propriedade : longint;

begin
    read(numero);
    propriedade := (numero - numero mod 100) div 100 + numero mod 100;
    if propriedade*propriedade = numero then
        writeln('SIM')
    else
        writeln('NAO')
end.
