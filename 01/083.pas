program idade_em_dias;

var dias, meses, anos : longint;

begin
    read(dias);
    anos := dias div 365;
    dias := dias mod 365;
    meses := dias div 30;
    dias := dias mod 30;
    writeln( anos,' ',meses,' ',dias )
end.
