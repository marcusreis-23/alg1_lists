program media_ponderada;

const N = 3;

var p,i,sum : longint;

begin
    sum := 0;
    for i:=1 to N do
    begin
        read(p);
        sum += p*i;
    end;
    writeln(sum/(N*(N-1)):0:0);
end.
