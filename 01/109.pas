program oposto_caso_no_intervalo;

var numero : longint;

begin
    read(numero);
    if (numero > -15) and (numero < 30) then
        writeln(-numero)
    else
        writeln(numero);
end.
