program volume_esfera;

const PI = 3.14;
var 
 diametro : longint;
 cubo_raio : real;

begin
    read(diametro);

    {* Volume de uma esfera: V = R^3 * 4PI/3 *}
    cubo_raio := (diametro/2);
    cubo_raio *= cubo_raio;
    cubo_raio *= cubo_raio;
    writeln(cubo_raio*4*PI/3:0:2);
end.
