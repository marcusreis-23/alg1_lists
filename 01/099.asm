%include "basefunctions.asm"

SECTION .bss
    years resb 3
    months resb 2
    days resb 2

SECTION .text
    global _start

_start:
    mov eax,3
    mov ebx,0
    mov ecx,years
    mov edx,3
    int 0x80 

    mov eax,3
    mov ebx,0
    mov ecx,months
    mov edx,2
    int 0x80 
    
    mov eax,3
    mov ebx,0
    mov ecx,days
    mov edx,2
    int 0x80 

    mov eax,days
    call atoi

    push eax

    mov eax,months
    call atoi
    
    mov ebx,30
    mul ebx
    push eax

    mov eax,years
    call atoi

    mov ebx,365
    mul ebx

    pop ebx

    add eax,ebx

    pop ebx

    add eax,ebx

    call iprintln

    call quitprogram
