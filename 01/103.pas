program height_based_on_cosin_and_length;

var 
 cosin : real;
 len   : real;
 dist  : longint;
begin
    read(cosin,dist);
    len := dist / cosin;
    writeln(sqrt(len*len-(dist*dist)):0:3);
end.
