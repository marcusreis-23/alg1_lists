program segundos_para_horas;

var segundos,minutos,horas : longint;

begin
    read(segundos);
    horas := segundos div 3600;
    segundos := segundos mod 3600;
    minutos := segundos div 60;
    segundos := segundos mod 60;
    writeln(horas,':',minutos,':',segundos);
end.
