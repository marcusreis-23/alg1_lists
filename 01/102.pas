program tres_para_quatro_digitos;

var
 numero : longint;
 quarto_digito : longint;

begin
    read(numero);
    quarto_digito := ((numero div 100) + 3*(numero mod 100 div 10) + 5*(numero mod 100 mod 10)) mod 7;
    numero := numero*10;
    writeln(numero+quarto_digito);
end.
