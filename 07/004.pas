program interesting_stuff;

const MAX = 200;
var 
(* "vetor" para entradas e "diferentes" para armazenar todos os numeros diferentes *)
 vetor,diferentes : array[0..MAX] of longint;
 lenv,lend : shortint; (* controle do tamanho dos vetores *)
 i,tot : shortint; (* variavel de iteracao e total de repeticoes *)

function is_in_array(var arr : array of longint; arrlen : shortint; value : longint) : boolean;
var i : shortint; (* iterador *)
begin
    for i:= 0 to arrlen-1 do
    begin
        is_in_array := arr[i] = value;
        (* sai do loop imediatamente se ja estiver no array *)
        if is_in_array then break;
    end;
end;

(* "aumenta o comprimento do array" e adiciona o item na posicao de indice mais alto *)
procedure append_to_array(var arr : array of longint; var arrlen : shortint; value : longint);
begin
    arr[arrlen] := value;
    arrlen += 1;
end;

(* conta quantas vezes um valor se repete num array *)
function count(var arr : array of longint; arrlen : shortint; value : longint) : shortint;
var i : shortint; (* iterador *)
begin
    count := 0;
    for i:=0 to arrlen-1 do
        if arr[i] = value then count += 1;
end;

begin
    read(lenv); 
    lend := 0;
    (* tamando do vetor ja eh predefinido, dispensando o uso de "append" *)
    for i := 0 to lenv-1 do
    begin
        read(vetor[i]);
        if not is_in_array(diferentes,lend,vetor[i]) then
            append_to_array(diferentes,lend,vetor[i]);
    end;
    if lend = 0 then
        writeln('vetor vazio')
    else if lend > 1 then
        write('a sequencia tem ',lend,' numeros distintos: ')
    else 
        write('a sequencia tem ',lend,' numero distinto: ');
    

    
    (* escreve todos os numeros distintos do vetor original *)
    for i := 0 to lend-1 do
        write(diferentes[i], ' ');
    writeln();

    for i := 0 to lend-1 do
    begin
        tot := count(vetor,lenv,diferentes[i]);
        write(diferentes[i],' ocorre ',tot,' vez');
        (* levar em consideracao o plural *)
        if tot > 1 then
            writeln('es')
        else
            writeln();
    end;
end.
