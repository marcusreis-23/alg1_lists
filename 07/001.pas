program somapositivos_dividido_por_somanegativos;

var 
    n,i : longint;
    tmp,somapos,somaneg : real;
begin
    read(n);
    somapos := 0;
    somaneg := 0;
    for i:=1 to n do
    begin
        read(tmp);
        (*  posicoes pares    maiores que zero *)
        if (i and 1 = 0) and (tmp > 0) then
            somapos += tmp
        else if (i and 1 <> 0) and (tmp < 0) then
            somaneg += tmp;
    end;
    if n = 0 then
        writeln('vetor vazio')
    else if somaneg = 0 then
        writeln('divisao por zero')
    else
        writeln(somapos/somaneg:0:2);
end.
