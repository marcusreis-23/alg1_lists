program sequencia_ordenada;

const MAX = 200;
var 
    vect : array[0..MAX] of longint;
    n,i,prev : longint;
    ordered : boolean;

begin
    ordered := true;
    read(n);    
    if n > 0 then
    begin
        read(vect[0]);
        prev := vect[0];
    end;
    for i:=1 to n-1 do
    begin
        read(vect[i]);
        if prev > vect[i] then
            ordered := false;
    end;
    if prev <> 0 then
    begin   
        if ordered then
            writeln('sim')
        else
            writeln('nao');
            writeln();
        for i:=n-1 downto 0 do
            if i=0 then
                writeln(vect[i])
            else
                write(vect[i],' ');
    end
    else
        writeln('vetor vazio');
end.
