program operations;

const MAX = 200;
var
 vetor : array[0..MAX] of real;
 lenv : byte;
 operation,what : byte;

procedure write_arr(var arr : array of real; arrlen : byte);
var i : byte;
begin
    if arrlen = 0 then
        write('vazio')
    else
        for i:=0 to arrlen-1 do
            write(vetor[i]:0:1, ' ');
    writeln();
end;

procedure input_data(var arr : array of real; var arrlen : byte);
var data : real; i,j : byte;
begin
    read(data);
    if arrlen = 0 then
    begin
        arr[0] := data;
        arrlen += 1;
        write_arr(arr,arrlen);
    end
    (* array nao deve ultrapassar 200 elementos *)
    else if arrlen >= 200 then
        writeln('erro')
    else
    begin
        (* insert data in appropriate position *)
        for i:=0 to arrlen-1 do
        begin
            if arr[i] >= data then
            begin
            (* mover elementos maiores que o input 1 casa para frente *)
                for j:=arrlen downto i+1 do
                    arr[j] := arr[j-1];
                arr[i] := data;
                break;
            end
            else if i = arrlen-1 then
                arr[arrlen] := data;
                
        end;
        arrlen += 1;
        write_arr(arr,arrlen);
    end;
end;

function is_in(var arr : array of real; arrlen : byte; value : real) : boolean;
var i : byte;
begin
    for i:=0 to arrlen-1 do
    begin
        is_in := arr[i] = value;
        if is_in then break;
    end;
end;

procedure remove_data(var arr : array of real; var arrlen : byte);
var data : real; i,j : byte;
begin
    read(data);
    if not is_in(arr, arrlen, data) then
        writeln('erro')
    else
    begin
        for i:=0 to arrlen-1 do
        begin
            if arr[i] = data then
            begin
            (* mover elementos na frente do input uma casa para tras *)
                for j:=i to arrlen-1 do
                    arr[j] := arr[j+1];
                arrlen -= 1;
                break;
            end;
        end;
        write_arr(arr,arrlen);
    end;
end;

begin
    lenv := 0;
    while true do
    begin
        read(operation);
        case (operation) of
            0: break;
            1: input_data(vetor,lenv);
            2: remove_data(vetor,lenv);
        end;
    end;
    write_arr(vetor,lenv);
end.
