program subsequence_of_input;

const MAX = 255;
var 
 sequence,subsequence : array[0..MAX] of longint;
 lenseq,lensub : byte;
 i : byte;

function is_subsequence(var seq,sub : array of longint; seql,subl : byte) : boolean;
var position,i : byte;
begin
    is_subsequence := false; position := 0;
    while (not is_subsequence) and (position+subl<=seql) do
    begin
        for i:=0 to subl-1 do
        begin
            if sub[i] <> seq[position+i] then
            begin
                position += 1;
                break; 
            end
            else if (sub[i] = seq[position+i]) and (i=subl-1) then
                is_subsequence := true;
        end;
    end;
end;

begin
    read(lenseq,lensub);
    if lenseq = 0 then
        writeln('sequencia inicial vazia')
    else if lensub = 0 then
        writeln('sequencia sendo procurada vazia')
    else
    begin
        for i:=0 to lenseq-1 do
            read(sequence[i]);
        for i:=0 to lensub-1 do
            read(subsequence[i]);
        if lenseq < lensub then
            writeln('nao')
        else if is_subsequence(sequence,subsequence,lenseq,lensub) then
            writeln('sim')
        else 
            writeln('nao');
    end;
end.
