program contrario;

var n,m : longint;

function inverte(num : longint) : longint;
begin
    inverte := 0;
    while num > 0 do
    begin
        inverte *= 10;
        inverte += num mod 10;
        num := num div 10;
    end;
end;

(* nome da funcao era o mesmo do programa *)
function contrarios(a,b : longint) : boolean;
begin
    contrarios := a = inverte(b);
end;

begin
    read (n,m);
    if contrarios(n,m) then
        writeln (n,' eh o contrario de ',m)
    else
        writeln (n,' nao eh o contrario de ',m);
end.
