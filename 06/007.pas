program incrementa_uma_unidade;

const TOP = 10;
var n : longint;

procedure incrementa(var num : longint);
begin
    (* inc(num); lol *)
    num += 1;
end;

begin
    n := 1;
    while n <= TOP do
    begin
        writeln(n);
        incrementa(n);
    end;
end.
