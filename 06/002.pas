program testa_binario;

var n : longint;

function eh_binario(num : longint) : boolean;
begin
    (* por padrao assume-se que eh verdade *)
    eh_binario := true;
    while num <> 0 do
    begin
        (* se o digito menos significativo nao eh 1 nem 0 aka ">1" *)
        if num mod 10 > 1 then
        begin
            eh_binario := false;
            (* sair do loop *)
            break;
        end;
        (* excluir digito menos significativo *)
        num := num div 10;
    end;
end;
begin
    read(n);
    if eh_binario(n) then
        writeln('sim')
    else
        writeln('nao');
end.
