program converte;
uses math;

var n : longint;

function converte_em_decimal(num : longint) : longint;
var cont : shortint;
begin
    cont := 0;
    converte_em_decimal := 0;
    while num <> 0 do
    begin
        if num mod 10 <> 0 then
            converte_em_decimal += Trunc(intpower(2, cont));
        (* proximo digito *)
        num := num div 10; 
        cont += 1;
    end;
end;

begin
    read(n);
    writeln(converte_em_decimal(n));
end.
