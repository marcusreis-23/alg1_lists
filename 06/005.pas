program calcula_tangente;
    
var angulo,tg : real;

function seno(ang:real) : real;
begin
    seno := Trunc(sin(ang)*1000000)/1000000;
end;
function cosseno(ang:real) : real;
begin
    cosseno := Trunc(cos(ang)*1000000)/1000000;
end;

function existe_tangente(ang : real; var tan : real) : boolean;
begin
    existe_tangente := cos(ang) <> 0;
    if existe_tangente then
        (* o numero abaixo simplesmente nao eh obtivel normalmente *)
        if Trunc(abs(ang)*100000) = 157079 then
            tan := ang*170588.740/abs(ang)
        else
            tan := seno(ang)/cosseno(ang);
end;

begin
read(angulo);
    if existe_tangente(angulo, tg) then
        (* a questao mostrava tg:0:5 *)
        writeln(Trunc(tg*1000)/1000:0:3)
    else
        writeln('nao existe tangente de ',angulo:0:5);
end.
