program compara_datas;

var dia1, mes1, ano1, dia2, mes2, ano2: longint;

function eh_anterior(d0,m0,a0,d1,m1,a1 : longint) : boolean;
begin
    eh_anterior := (a0 < a1) or ((a0 = a1) and (m0 < m1)) or ((a0 = a1) and (m0 = m1) and (d0 < d1));
end;
begin
    read (dia1, mes1, ano1, dia2, mes2, ano2);
    if eh_anterior (dia1, mes1, ano1, dia2, mes2, ano2) then
        writeln ('a primeira data eh anterior')
    else
        writeln ('a primeira data nao eh anterior');
end.
