program testa_se_primo;

const top = 10000;
var i : longint;

function eh_primo(n : longint) : boolean;
var i : longint;
begin
    if n and 1 = 0 then
        eh_primo := false
    else
    begin
        i := 2;
        while i < n/2 do
        begin
            eh_primo := n mod i <> 0;
            if not eh_primo then
                break;
            i+=1;
        end;
    end;
end;


begin
    (* casos base *)
    writeln(1); (* nota: 1 nao eh primo *)
    writeln(2);
    writeln(3);
    i := 3;
    while i < TOP do
    begin
        if eh_primo(i) then
            writeln(i);
        i+=2;
    end;
end.
